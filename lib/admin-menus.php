<?php

add_action( 'admin_menu', 'omneo_settings_page' );


acf_add_options_page(array(
    'page_title' 	=> 'Core Settings',
    'menu_title'	=> 'Core Settings',
    'menu_slug' 	=> 'core-settings',
    'capability'	=> 'activate_plugins',
    'redirect'		=> false
));


function omneo_settings_page()
{

    $omneo_page = add_menu_page( 'Webhooks', 'Webhooks', 'manage_options', 'omneo-core/webhooks/index.php', '', '' );

    add_action( 'load-' . $omneo_page, function(){die();} );


}



