<?php


function create_user_password_rest_page()
{
    $title = 'Password Reset';

    $is_rest_page = get_page_by_title($title , OBJECT, 'page' );

    if(empty($is_rest_page))
    {

        $post_data = array(
            'post_title' => $title,
            'post_content' => '',
            'post_type' => 'page',
            'post_status' => 'publish'

        );

        $post_id = wp_insert_post( $post_data, true);
    }
}

add_action( 'admin_init', 'create_user_password_rest_page' );

function create_user_password_updated_page()
{
    $title = 'Password Updated';

    $is_rest_page = get_page_by_title($title , OBJECT, 'page' );

    if(empty($is_rest_page))
    {

        $post_data = array(
            'post_title' => $title,
            'post_content' => '',
            'post_type' => 'page',
            'post_status' => 'publish'

        );

        $post_id = wp_insert_post( $post_data, true);
    }
}

add_action( 'admin_init', 'create_user_password_updated_page' );

//==================== PASSWORD RESET ==================== //
function reset_user_password()
{

    if(strstr($_SERVER['REQUEST_URI'], '/password-reset'))
    {
        if($_POST['token'])
        {
            //var_dump($_POST);

            $password =  sanitize_key($_POST['password']);


            if($password != sanitize_key($_POST['password-re']))
           {
             header('location:?token=' . $_POST['token'] .'&password=nomatch' );
           }else
            {
                $token = sanitize_key($_GET['token']);

               /* //check the key just in case
                $data = array();
                $data['api_request'] = 'userattributes';
                $data['verb'] = 'get';
                $data['data']['key'] = 'password_reset_token';
                $data['data']['value'] = ($token) ? $token : md5(time());

                $post_response = \Omneo\Core\send_request($data);*/

/*                if ($post_response['data'][0]['user_id'])
                {
                    $data = array();
                    $data['api_request'] = 'passwordrequestreset/' . $token;
                    $data['verb'] = 'put';
                    $data['data']['users'] = $password;

                    $put_response = \Omneo\Core\send_request($data);

                    header('location:/password-updated');
                } else
                {
                    die('Not a valid token');
                }*/


                $data = array();
                $data['api_request'] = 'passwordrequestreset/' . $token;
                $data['verb'] = 'put';
                $data['data']['password'] = $password;

                $put_response = \Omneo\Core\send_request($data);
                if(($put_response['error']))
                {
                    header('location:?token='.$token .'&msg=' . $put_response['error']['message']);
                }
                else
                {
                    header('location:/password-updated');exit;
                }

                 die();



            }

        }


       //CANNOT QUERY A USER BY TOKEN YET
       /* $data = array();
        $data['api_request'] = 'userattributes';
        $data['verb'] = 'get';
        $data['data']['key'] = 'password_reset_token';
        $data['data']['value'] = (sanitize_key($_GET['token'])) ? sanitize_key($_GET['token']) : md5(time());


        $response = \Omneo\Core\send_request($data);


        if(!$response['data'][0]['user_id'])
        {
            die('Not a valid token');
        }*/
    }
}

add_action('parse_request', __NAMESPACE__ . '\\reset_user_password');