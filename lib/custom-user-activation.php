<?php


$org_domains = array(
    'arkade.com.au'

);

function create_user_account_approve_page()
{
    $title = 'Activate Account';

    $is_rest_page = get_page_by_title( $title, OBJECT, 'page' );

    if(empty($is_rest_page))
    {

        $post_data = array(
            'post_title' => $title,
            'post_content' => '',
            'post_type' => 'page',
            'post_status' => 'publish'

        );

        $post_id = wp_insert_post( $post_data, true);
    }
}
add_action( 'admin_init', 'create_user_account_approve_page' );


function create_user_account_activated_page()
{
    $title = 'Account Activated';

    $is_rest_page = get_page_by_title( $title, OBJECT, 'page' );

    if(empty($is_rest_page))
    {

        $post_data = array(
            'post_title' => $title,
            'post_content' => '',
            'post_type' => 'page',
            'post_status' => 'publish'

        );

        $post_id = wp_insert_post( $post_data, true);
    }
}

add_action( 'admin_init', 'create_user_account_activated_page' );

//==================== USER APPROVE ==================== //
function approve_user_account()
{

    if(strstr($_SERVER['REQUEST_URI'], '/activate-account'))
    {
        //var_dump($_GET);
        if($_POST['token'])
        {
            //check the key just in case
            $data = array();
            $data['api_request'] = 'userattributes';
            $data['verb'] = 'get';
            $data['data']['key'] = 'approval_token';
            $data['data']['value'] = (sanitize_key($_GET['token'])) ? sanitize_key($_GET['token']) : md5(time());
            $post_response = \Omneo\Core\send_request($data);

            if($post_response['data'][0]['user_id'])
            {
                $data = array();
                $data['api_request'] = 'users/'. $post_response['data'][0]['user_id'];
                $data['verb'] = 'put';
                $data['id'] = $post_response['data'][0]['user_id'];
                $data['data']['approval_token'] = '';
                $data['data']['approved'] = true;
                $data['data']['role_id'] = 5;
                $put_response = \Omneo\Core\send_request($data);



                header('location:/account-activated');
            }else
            {
                die('Not a valid token');
            }

        }

        $data = array();
        $data['api_request'] = 'userattributes';
        $data['verb'] = 'get';
        $data['data']['key'] = 'approval_token';
        $data['data']['value'] = (sanitize_key($_GET['token'])) ? sanitize_key($_GET['token']) : md5(time());


        $response = \Omneo\Core\send_request($data);

//        var_dump($response);

        if(!$response['data'][0]['user_id'])
        {
            die('Not a valid token');
        }
    }
}

add_action('parse_request', __NAMESPACE__ . '\\approve_user_account');