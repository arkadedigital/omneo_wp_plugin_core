<?php

function remove_dashboard_meta()
{
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
    remove_meta_box('dashboard_primary', 'dashboard', 'normal');
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
    remove_meta_box('dashboard_activity', 'dashboard', 'normal');
}

add_action('admin_init', 'remove_dashboard_meta');


/* Add a widget in WordPress Dashboard */
function omneo_dashboard_widget_welcome()
{
    ?>
    <div class="admin-logo">
        <a href="http://www.omneo.com.au" target="_blank">
            <img src="<?php echo plugins_url('omneo-core/assets/omneo-logo.png'); ?>"
                 alt="Arkade Digital">
        </a>
    </div>

    <p>Welcome to your Control Panel. You can use this control panel to manage every aspect of your App. </p>
    <p>Take a moment to familiarise yourself with the layout. Pay particular attention to the main menu options on the
        left of the page.</p>

    <p>Each of these options contains a sub-menu giving you easy access to every management
        tool at your disposal.
    </p>
    <div class="clear"></div>
    <?php
}


// Add a quick access widget in WordPress Dashboard
function omneo_dashboard_widget_quickaccess()
{
    ?>
    <div id="quick-access">
        <ul>
            <li>
                <a href="/wp-admin/edit.php?post_type=views">
                    <span><div class="wp-menu-image dashicons-before dashicons-visibility"><br></div></span>
                    Views</a>
            </li>
            <li>
                <a href="/wp-admin/edit.php?post_type=content_items">
                    <span><div class="wp-menu-image dashicons-before dashicons-smartphone"><br></div></span>
                    Content Items</a>
            </li>
            <li>
                <a href="/wp-admin/admin.php?page=segments">
                    <span><div class="wp-menu-image dashicons-before dashicons-editor-ol"><br></div></span>
                    Segments</a>
            </li>
            <li>
                <a href="/wp-admin/admin.php?page=push-notifications">
                    <span><div class="wp-menu-image dashicons-before dashicons-email-alt"><br></div></span>
                    Push Notifications</a>
            </li>
            <li>
                <a href="/wp-admin/admin.php?page=app-users">
                    <span><div class="wp-menu-image dashicons-before dashicons-admin-users"><br></div></span>
                    Users</a>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    <?php
}


function omneo_dashboard_widget_support()
{
    ?>
    <p>Send us a message for any issue that you have been facing and you are unable to solve. Please <a
            href="http://www.omneo.com.au" target="_blank">say Hello</a> at any time and we may
        help you to solve it.</p>

    <div class="clear"></div>
    <?php
}

function omneo_dashboard_widget_release()
{
    $output = '
        <table class="wp-list-table widefat striped">
        <tr>
        <td>WP_ENV</td><td>' . WP_ENV . '</td>
        </tr>
        </table><br>
    ';


    $output .= '<table class="wp-list-table widefat striped"><thead><tr><th>Plugin</th><th>Version</th></tr></thead><tbody>';
    foreach (get_plugins() as $plugin) {
        $output .= '<tr>';
        $output .= '<td>' . $plugin['Name'] . '</td>';
        $output .= '<td>' . $plugin['Version'] . '</td>';
        $output .= '</tr>';
    }
    $output .= '</tbody></table>';

    echo $output;
}


/**
 * Custom admin dashboard widgets
 */
add_action('wp_dashboard_setup', __NAMESPACE__ . '\\omneo_add_dashboard_widgets');
function omneo_add_dashboard_widgets()
{
    add_meta_box('wp_dashboard_widget', 'Welcome to Omneo', __NAMESPACE__ . '\\omneo_dashboard_widget_welcome', 'dashboard', 'normal', 'core');

    // add the quick access widget if allowed
    if (is_user_logged_in()) {
        global $current_user;

        $user_roles = $current_user->roles;
        $role = array_shift($user_roles);

        if (($role == 'editor') || ($role == 'administrator')) {
            add_meta_box('wp_menu_widget', __('Quick Access', ''), __NAMESPACE__ . '\\omneo_dashboard_widget_quickaccess', 'dashboard', 'side', 'core');

        }
    }
    add_meta_box('wp_support_widget', __('Support', ''), __NAMESPACE__ . '\\omneo_dashboard_widget_support', 'dashboard', 'side', 'core');
    add_meta_box('wp_release_widget', __('Technical information', ''), __NAMESPACE__ . '\\omneo_dashboard_widget_release', 'dashboard', 'normal', 'core');
}

