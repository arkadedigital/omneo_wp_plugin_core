<?php

use Omeno\Core;

// Set default s3 bucket if one is not set
// Updates s3 bucket if env AWS_S3_BUCKET has changed
function set_default_s3_bucket()
{
    $s3 = get_option('tantan_wordpress_s3');

    if (empty($s3['bucket'])) {
        if (getenv('AWS_S3_BUCKET')) {
            $arr = [
                'post_meta_version'     => 3,
                'bucket'                => getenv('AWS_S3_BUCKET'),
                'region'                => getenv('AWS_S3_REGION') ?: 'ap-southeast-2',
                'domain'                => 'subdomain',
                'expires'               => 0,
                'cloudfront'            => '',
                'object-prefix'         => 'uploads/',
                'copy-to-s3'            => 1,
                'serve-from-s3'         => 1,
                'remove-local-file'     => 0,
                'ssl'                   => 'request',
                'hidpi-images'          => 0,
                'object-versioning'     => 1,
                'use-yearmonth-folders' => 1,
                'enable-object-prefix'  => 1
            ];

            update_option('tantan_wordpress_s3', $arr);
        }
    } elseif (getenv('AWS_S3_BUCKET') != $s3['bucket']) {
        // Update bucket if env value AWS_S3_BUCKET has changed
        $s3['bucket'] = getenv('AWS_S3_BUCKET');
        update_option('tantan_wordpress_s3', $s3);
    }
}

add_action('admin_init', __NAMESPACE__ . '\\set_default_s3_bucket');


function is_s3_bucket_set()
{
    $bucket = get_option('tantan_wordpress_s3');
    //var_dump($bucket);
    if (empty($bucket['bucket'])) {
        add_action('admin_notices', function () {
            echo "<div class='error'> <p>
            <h1>No S3 bucket set</h1>
            <h3>Do Not Upload media files if you can see this error</h3>
            <a href='/wp-admin/admin.php?page=amazon-s3-and-cloudfront'>Please set a S3 bucket in AWS -> S3 And CouldFront</a>
            </p></div>";
        });
    }
    //tantan_wordpress_s3
}

add_action('admin_init', __NAMESPACE__ . '\\is_s3_bucket_set');


function remove_admin_menu()
{
    remove_menu_page('edit.php');
    remove_menu_page('edit.php?post_type=page');
    remove_menu_page('edit-comments.php');
    remove_menu_page('tools.php');
    remove_menu_page('themes.php');

}

add_action('admin_menu', __NAMESPACE__ . '\\remove_admin_menu');


function fb_disable_feed()
{
    wp_die(__('No feed available,please visit our <a href="' . get_bloginfo('url') . '">homepage</a>!'));
}

add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'fb_disable_feed', 1);
add_action('do_feed_atom_comments', 'fb_disable_feed', 1);


function admin_color_theme()
{
    wp_admin_css_color(
        'omneo',
        __('Omeno'),
        plugins_url() . '/omneo-core/assets/wp-admin.css',
        array('#16B0A3', '#0E675F', '#16B0A3', '#16B0A3')
    );
    remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');
}

add_action('admin_init', 'admin_color_theme');

function set_default_admin_color()
{
    $user_id = get_current_user_id();

    $args = array(
        'ID'          => $user_id,
        'admin_color' => 'omneo'
    );
    wp_update_user($args);
}

add_action('admin_init', 'set_default_admin_color');


function get_current_config_from_api()
{
    $args = [
        'api_request' => 'config',
        'verb'        => 'get'
    ];
    $response = \Omneo\Core\send_request($args);
    $update_response = [];

    if ($response['data']) {

        foreach ($response['data'] as $r) {
            $r['omneo_id'] = $r['id'];
            $r['g_config_key'] = $r['key'];
            $r['g_config_value'] = $r['value'];

            unset($r['id']);
            unset($r['created_at']);
            unset($r['updated_at']);
            unset($r['deleted_at']);

            $update_response[] = $r;
        }
    }

    return $update_response;
}

if (@$_GET['page'] == 'core-settings') {
    update_field('field_561d864020246', get_current_config_from_api(), 'option');
}


if (!empty($_POST['acf']['field_561d864020246'])) {
    $configs = $_POST['acf']['field_561d864020246'];


    foreach ($configs as $config) {
        $args = [
            'api_request' => 'config/' . $config['field_561d98925118d'],
            'verb'        => 'PUT'
        ];

        $args['data'] = array(
            'value' => $config['field_561d867020248']
        );
        $response = \Omneo\Core\send_request($args);
    }

    wp_redirect('?page=core-settings');


    //var_dump($configs); die();
}


function activate_omneo_theme()
{
    $theme = 'omneo';

    update_option('template', $theme);
    update_option('stylesheet', $theme);
    update_option('current_theme', $theme);

}

register_activation_hook(\Omneo\Core\__PATH() . 'omneo-core.php', 'activate_omneo_theme');


/**
 * Remove admin screen and help tab
 */
//add_filter('screen_options_show_screen', '__return_false');
add_action('admin_head', __NAMESPACE__ . '\\remove_help_tab');
function remove_help_tab()
{
    $screen = get_current_screen();
    $screen->remove_help_tabs();
}

/**
 * Remove WordPress welcome panel
 */
remove_action('welcome_panel', 'wp_welcome_panel');


/**
 * Customize admin bar
 */
add_action('admin_bar_menu', __NAMESPACE__ . '\\omneo_admin_bar_menu', 999);
function omneo_admin_bar_menu($wp_admin_bar)
{
    // Remove nodes
    $wp_admin_bar->remove_node('site-name');
    $wp_admin_bar->remove_node('view');
    $wp_admin_bar->remove_node('my-account');
    $wp_admin_bar->remove_node('comments');
    $wp_admin_bar->remove_node('updates');
    $wp_admin_bar->remove_node('new-content');

    // Main title
    if (getenv('WP_ENV') !== 'production') {
        $wp_admin_bar->add_menu(array(
            'id'    => 'dashboard',
            'title' => '<span style="font-weight:bold;font-size:18px;text-transform:capitalize;background:#f00;display:block;left:-6px;position:relative;padding:0 10px;">Omneo: ' . getenv('WP_ENV') . ' Environment</span>',
            'href'  => '/wp-admin'
        ));
    } else {
        $wp_admin_bar->add_menu(array(
            'id'    => 'dashboard',
            'title' => '<span style="font-weight:bold;font-size:18px;">Omneo</span>',
            'href'  => '/wp-admin'
        ));

    }

    // Logout
    $wp_admin_bar->add_menu(array(
        'id'     => 'logout',
        'parent' => 'top-secondary',
        'title'  => '<span class="ab-icon dashicons dashicons-migrate"></span> Logout',
        'href'   => wp_logout_url()
    ));


    if ($user_id = get_current_user_id()) {
        $current_user = wp_get_current_user();
        $profile_url = get_edit_profile_url($user_id);

        $wp_admin_bar->add_menu(array(
            'id'     => 'profile',
            'parent' => 'top-secondary',
            'title'  => '<span class="ab-icon dashicons dashicons-admin-users"></span>' . $current_user->display_name,
            'href'   => $profile_url,
        ));
    }
}

// Remove admin bar
add_filter('show_admin_bar', '__return_false');


// Check if htaccess exists for rewrites to work
function is_htaccess_generated()
{
    if (strpos($_SERVER['SERVER_SOFTWARE'], 'nginx') === false) {
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/.htaccess')) {
            add_action('admin_notices', function () {
                echo "<div class='error'> 
                <p>.htaccess file is missing.</p>
                <p><a href='/wp-admin/options-permalink.php'>Click here</a> to generate a new .htaccess file.</p>
            </div>";
            });
        }
    }
}

add_action('admin_init', __NAMESPACE__ . '\\is_htaccess_generated');