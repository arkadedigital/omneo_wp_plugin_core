<?php

namespace Omeno\Core\HMAC;

function generate_hmac_signature($method, $route, $payload, $requestTimeLimit = 300)
{
    $accessKey = get_field('hamc_accesskey', 'option');
    $accessKeySecret = get_field('hamc_accesssecret', 'option');
    $version =    get_field('x-ark-version', 'option');

    /*
    $signed = [
        'X-ark-credential'   => $accessKey,
        'X-ark-timestamp' => time(),
    ];
    $method = strtolower($method);

    if(!empty($payload)){
        $payload = array_merge($signed,$payload);
    }else{
        $payload = $signed;
    }

    $payloadString = urldecode(http_build_query($payload));

    $signingKey = hash_hmac(HMAC_ALGO, strtolower($method), $accessKeySecret,true);
    $signingKey = hash_hmac(HMAC_ALGO, strtolower($route), $signingKey,true);
    $signingKey = hash_hmac(HMAC_ALGO, 'ARK_REQUEST', $signingKey,true);
    $signature  = hash_hmac(HMAC_ALGO, $payloadString, $signingKey);

    $payload['X-ark-signature'] = $signature;

    return $payload;
    */

    $signed = [
        'X-ark-credential'   => $accessKey,
        'X-ark-timestamp' => time(),
    ];

    if(!empty($payload)){
        $payload = array_merge($signed,$payload);
    }else{
        $payload = $signed;
    }

    $payloadString = urldecode(http_build_query($payload));

    $signingKey = hash_hmac('sha256', strtolower($method), $accessKeySecret);
    $signingKey = hash_hmac('sha256', strtolower($route), $signingKey);
    $signingKey = hash_hmac('sha256', 'ARK_REQUEST', $signingKey);
    $signature = hash_hmac('sha256', $payloadString, $signingKey);

    $payload['X-ark-signature'] = $signature;
    $payload['X-ark-version'] = $version;

    return $payload;
}
