<?php
function loggly($msgs = array(), $tag='omneo-wordpress')
{
    $ch = curl_init();
    $url = LOGGLY_URL .'/'. $tag;


    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($msgs));

    $content = trim(curl_exec($ch));

    curl_close($ch);

    return $content;
}

function write_to_log($str)
{
    $file = \Omneo\Core\__PATH() . 'log.txt';

    $dbt = debug_backtrace();

    if(is_array($str))
    {
        $str = print_r($str,  true);
    }

    $out = PHP_EOL;
    $out .= '[' . date("F j, Y, g:i:sa") .']' . PHP_EOL;

    if(!empty($dbt[1]['line']))
        $out .= '[Line : ' . $dbt[1]['line'] .']' . PHP_EOL;

    if(!empty($dbt[1]['file']))
        $out .= '[File : ' . $dbt[1]['file'] .']' . PHP_EOL;

    $out .= $str . PHP_EOL;


    file_put_contents($file, $out, FILE_APPEND);
}


function log_email($str)
{
    $file = \Omneo\Core\__PATH() . 'email.txt';

    //$dbt = debug_backtrace();

    if(is_array($str))
    {
        $str = print_r($str,  true);
    }

    $out = PHP_EOL;
    $out .= '[' . date("F j, Y, g:i:sa") .']' . PHP_EOL;

/*
    if(!empty($dbt[1]['line']))
        $out .= '[Line : ' . $dbt[1]['line'] .']' . PHP_EOL;

    if(!empty($dbt[1]['file']))
        $out .= '[File : ' . $dbt[1]['file'] .']' . PHP_EOL;*/

    $out .= $str . PHP_EOL;


    file_put_contents($file, $out, FILE_APPEND);
}
