<?php

namespace Omneo\Core;
require_once(WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'advanced-custom-fields-pro' . DIRECTORY_SEPARATOR . 'acf.php');

/**
 * Plugin Name:     Omneo Core
 * Plugin URI:      http://www.omneo.com.au
 * Description:     Omneo Core
 * Version:         1.0.16
 */


add_action('init', __NAMESPACE__ . '\\is_valid_api_url');
add_action('activated_plugin', __NAMESPACE__ . '\\activate_core_first');

define('DS', DIRECTORY_SEPARATOR);
define('HMAC_ALGO', 'sha256');
define('HMAC_KEY', 'omneo_hmac_key');
define('API_URL', \get_field('omneo_api_url', 'option') . '/api');
define('API_VERSION', \get_field('omneo_api_version', 'option'));
define('LOGGLY_URL', 'http://logs-01.loggly.com/inputs/6ae421d4-aef0-44b0-8e26-feb7e18037c3/tag');


function __PATH()
{
    return plugin_dir_path(__FILE__);
}

register_acf_from_json(__PATH());


function __load()
{
    require_once(__PATH() . 'lib/hmac.php');
    require_once(__PATH() . 'lib/logging.php');
    require_once(__PATH() . 'lib/email.php');
    require_once(__PATH() . 'lib/admin-menus.php');
    require_once(__PATH() . 'lib/dashboard.php');
    require_once(__PATH() . 'lib/global-config.php');
    require_once(__PATH() . 'webhooks/main.php');

    //custom features
    require_once(__PATH() . 'lib/custom-password-reset.php');
    require_once(__PATH() . 'lib/custom-user-activation.php');
}

add_action('after_setup_theme', __NAMESPACE__ . '\\__load');

///USERS STUFF
function create_omneo_user()
{
    $user_name = 'OmneoUser';
    $user_email = $user_name . '@' . $_SERVER['SERVER_NAME'];
    $user_pass = 'test123';

    $user_id = username_exists($user_name);
    if (!$user_id and email_exists($user_email) == false) {
        $user_id = wp_create_user($user_name, $user_pass, $user_email);
    }

    wp_update_user(array('ID' => $user_id, 'role' => 'editor'));
}

register_activation_hook(__FILE__, 'create_omneo_user');


// Custom admin footer
function omneo_custom_admin_footer()
{
    _e('<span id="footer-thankyou">Powered by OMNEO');
}

add_filter('admin_footer_text', __NAMESPACE__ . '\\omneo_custom_admin_footer');


register_activation_hook(__FILE__, function () {

    $list = array(
        'omneo-content',
        'omneo-locations',
        'omneo-notifications',
        'omneo-segments',
        'omneo-users',
        'omneo-views',
        'omneo-pulses',
        'wp-all-import-pro',
    );

    foreach ($list as $l) {
        activate_plugin($l . DS . $l . '.php');

    }
    activate_plugin('amazon-s3-and-cloudfront/wordpress-s3.php');
    activate_plugin('amazon-web-services/amazon-web-services.php');
    activate_plugin('advanced-custom-fields-pro/acf.php');
});

function register_acf_from_json($path)
{
    if (function_exists("register_field_group")) {
        $acf_path = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'acf';

        $acf_json_files = scandir($acf_path);
        unset($acf_json_files[0]); //remove .
        unset($acf_json_files[1]); //remove ..

        foreach ($acf_json_files as $acf_file) {
            if (strstr($acf_file, 'acf-export')) {

                $acf_json = file_get_contents($acf_path . DIRECTORY_SEPARATOR . $acf_file);
                $acf_json_decoded = json_decode($acf_json, true);
                $acf_json_decoded = $acf_json_decoded[0];
                $acf_json_decoded + array('id' => $acf_json_decoded['key']);
                unset($acf_json_decoded['key']);

                register_field_group($acf_json_decoded);
            }
        }

    }
}


/**
 * Sends request to the Omneo API
 * @param array $data
 * @return array|mixed|object
 */
function send_request($data = array())
{
    try {

        if (!$data['verb']) {
            //die('missing verb');
        }

        $verb = strtolower($data['verb']);

        $api_route = API_VERSION . '/' . $data['api_request'];
        $request_url = API_URL . '/' . $api_route;


        if ($data['post_id']) {
            $omneo_id = get_field('omneo_id', $data['post_id']);

            if ($verb != 'delete' && $verb != 'get') {
                $verb = $omneo_id ? 'put' : 'post';
            }
        }


        $hmac_route = 'api/' . $api_route;

        $method = $verb;
        $payload = $data['data'];

        $data['data'] = \Omeno\Core\HMAC\generate_hmac_signature($method, $hmac_route, $payload, $requestTimeLimit = 300);


        if ($verb == 'get') {
            $hmac_string = http_build_query($data['data']);
            $request_url .= '?' . $hmac_string;
        }


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($verb));

        if (!empty($data['data']) && $data['data'] != '' && $verb != 'get') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data['data']));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        }

        $content = trim(curl_exec($ch));

        if ($content === false) {
            echo 'Curl error: ' . curl_error($ch);
            die();
        }

        curl_close($ch);

        write_to_log('============ API REQUEST ============' . PHP_EOL . ">>>>>>>>>>  " . $verb . ' => ' . $request_url);
        write_to_log('============ POST TO API ============' . PHP_EOL . print_r($data, true));

//        write_to_log('============ API RESPONSE ============' . PHP_EOL. json_decode(print_r($content, true), true));
        write_to_log('============ API RESPONSE ============' . PHP_EOL . print_r(json_decode($content, true), true));

        return json_decode($content, true);
    } catch (\Exception $e) {
        loggly(array('Exception' => $e, 'Data' => $data), 'omneo-wp-request');
    }

}


function activate_core_first()
{
    $path = str_replace(WP_PLUGIN_DIR . '/', '', __FILE__);
    if ($plugins = get_option('active_plugins')) {
        if ($key = array_search($path, $plugins)) {
            array_splice($plugins, $key, 1);
            array_unshift($plugins, $path);
            update_option('active_plugins', $plugins);
        }
    }
}


function is_valid_api_url()
{
    $api_url = get_field('omneo_api_url', 'option');

    list($status) = @get_headers($api_url);


    if ($api_url == '' ||
        empty($api_url) ||
        ($api_url == NULL) && (strpos($_SERVER['REQUEST_URI'], 'omneo-')) ||
        (strpos($status, '200') != true)
    ) {
        add_action('admin_notices', function () {
            echo "<div class='error'> <p>
            Omneo API Endpoint is Unavailable  <br>
            <a href='/wp-admin/admin.php?page=core-settings'>Please update sthe API URL</a>
            </p></div>";
        });

    }else
    {
        add_action('admin_notices', function () {
            echo "<div class='notice'> <p>
           API URL : ". get_field('omneo_api_url', 'option') ."
                       </p></div>";
        });
    }
}


/**
 * Add favicon
 */
function favicon()
{ ?>
    <link rel="shortcut icon" href="<?php echo plugins_url() ?>/omneo-core/assets/favicon.ico">
<?php }

add_action('wp_head', __NAMESPACE__ . '\\favicon');
add_action('admin_head', __NAMESPACE__ . '\\favicon');


function pdo()
{
    $pdoObject = new \PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $pdoObject->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    return $pdoObject;
}


function load_admin_assets()
{
    //add_action( 'admin_enqueue_scripts', __NAMESPACE__. '\\webhook_scripts' );
    wp_enqueue_style('omneo-core-css', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'styles.css');
    wp_enqueue_script('omneo-core-js', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'scripts.js', array('jquery'));
}

load_admin_assets();
//add_action( 'wp_enqueue_scripts', 'load_admin_assets' );
