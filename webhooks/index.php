<?php

if(!empty($_POST['title'] && $_POST['event'] && $_POST['url']))
{
    $data  = array(
        'title' => $_POST['title'],
        'event' => $_POST['event'],
        'url' => $_POST['url'],
    );

    $data['api_request'] = WEBHOOK_API_REQUEST;
    $data['data'] = $data;

    $list = \Omneo\Core\Webhooks\add_webhook($data);

}

if(!empty($_GET['action']) && $_GET['action'] == 'delete')
{
    \Omneo\Core\Webhooks\delete_webhook(array('id' => sanitize_title($_GET['id'])));
}

function omneo_settings_webhooks_page()
{

    $list = \Omneo\Core\Webhooks\get_subscribed_webhooks();

    $rows = '';
    if(!empty($list['data']))
    {
        foreach($list['data'] as $item)
        {
            $rows .= '<tr>';
            $rows .= '<td>' . $item['title'] .'</td>';
            $rows .= '<td>' . $item['event'] .'</td>';
            $rows .= '<td>' . $item['url'] .'</td>';
            $rows .= '<td> <a class="remove-webbook" href="?page=omneo-core/webhooks/index.php&action=delete&id='. $item['id']. '" omneo-id="' . $item['id'] .'">Remove</a></td>';
            $rows .= '</tr>';
        }
    }else
    {
        $rows = '<tr> <td colspan="3">No Hooks Registred</td> </tr>';
    }


    return $rows;
}

?>
<div id="poststuff">

    <form action="" method="post" class="form-inline">

        <div id="acf-" class="postbox  acf-postbox default ">

            <div class="handlediv" title="Click to toggle"><br></div>
            <h3 class="hndle"><span>Add a new Webhook</span></h3>
            <div class="inside acf-fields acf-cf">

                <div class="acf-field acf-field-text" style="width: 30%; float: left;">
                    <div class="acf-label">
                        <label for="title">Title</label>
                    </div>
                    <div class="acf-input">
                        <div class="acf-input-wrap">
                            <input type="text" id="title" class="" name="title" value="" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="acf-field acf-field-text" style="width: 30%; float: left;">
                    <div class="acf-label">
                        <label for="event">Event</label>
                    </div>
                    <div class="acf-input">
                        <div class="acf-input-wrap">
                            <select name="event" id="">
                                <option value="omneo.omneo_users_registration.store">omneo.omneo_users_registration.store</option>
                                <option value="omneo.omneo_content_links.store">omneo.omneo_content_links.store</option>
                                <option value="omneo.omneo_content_links.update">omneo.omneo_content_links.update</option>
                                <option value="omneo.omneo_content_html.store">omneo.omneo_content_html.store</option>
                                <option value="omneo.omneo_content_html.update">omneo.omneo_content_html.update</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="acf-field acf-field-text" style="width: 30%; float: left;">
                    <div class="acf-label">
                        <label for="event">URL</label>
                    </div>
                    <div class="acf-input">
                        <div class="acf-input-wrap">
                            <select name="url" id="">
                                <option value="<?php echo site_url() . '/webhook/content/link'?>"><?php echo site_url() . '/webhook/content/link'?></option>
                                <option value="<?php echo site_url() . '/webhook/content/html'?>"><?php echo site_url() . '/webhook/content/html'?></option>
                                <option value="<?php echo site_url() . '/webhook/user/activateuser'?>"><?php echo site_url() . '/webhook/user/activateuser'?></option>
                                <option value="<?php echo site_url() . '/webhook/user/passwordreset'?>"><?php echo site_url() . '/webhook/user/passwordreset'?></option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="acf-field acf-field-text" style="width: 30%; float: left;">
                    <button type="submit" class="button button-primary">Add</button>
                </div>



            </div>
        </div>
    </form>

    <div id="acf-" class="postbox  acf-postbox default ">
        <div class="inside acf-fields acf-cf">
            <div class="inside acf-fields acf-cf">


                <table class="table table-striped" cellpadding="5" width="100%">
                    <thead>
                    <tr>
                        <td width="20%">Name</td>
                        <td width="30%">Event</td>
                        <td width="40%">URL</td>
                        <td width="10%">&nbsp;</td>
                    </tr>
                    </thead>
                    <tbody>

                    <?php echo omneo_settings_webhooks_page()?>
                    </tbody>
                </table>
            </div>


            </div>
            </div>
    </div>
</div>


