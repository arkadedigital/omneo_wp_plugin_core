<?php

namespace Omneo\Core\Webhooks;
use Omneo\Core;
use Omneo\Content\Item\Html;
use Omeno\Users;


define('WEBHOOK_API_REQUEST', 'webhooks');




function create_tables()
{
    $sql = "CREATE TABLE IF NOT EXISTS `webhooks` (
              `id` int(11) NOT NULL,
              `omneo_id` int(11) NOT NULL,
              `name` varchar(255) DEFAULT NULL,
              `url` varchar(255) DEFAULT NULL,
              `status` tinyint(1) DEFAULT '0',
              `last_updated` datetime DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

    try
    {
        $conn = Core\pdo();
        $conn->exec($sql);;
    }
    catch (Exception $e)
    {
        echo $e->getMessage();
    }
}

function get_subscribed_webhooks()
{
    $data['api_request'] = WEBHOOK_API_REQUEST;
    $data['data'] = array();
    $data['verb'] = 'GET';

    $list = Core\send_request($data);


    return $list;

}

function  add_webhook($post_data)
{

    $data['api_request'] = WEBHOOK_API_REQUEST;
    $data['data'] = $post_data;
    $data['verb'] = 'POST';


    $response = Core\send_request($data);

    if(!empty($response['error']))
    {
        Core\loggly($response, 'omneo-wp-webhook');
        $class = "alert alert-danger";
        $message = $response['error']['message'];
        echo"<div class=\"$class\"> <p>$message</p></div>";
    }

    //var_dump($response);
}



function delete_webhook($post_data)
{
    $data['api_request'] = WEBHOOK_API_REQUEST . '/' . $post_data['id'];
    $data['data'] = array('id' => $post_data['id']);
    $data['verb'] = 'DELETE';


    $response = Core\send_request($data);

    if(!empty($response['error']))
    {
        loggly($response, 'omneo-wp-webhook');
        $class = "alert alert-danger";
        $message = $response['error']['message'];
        echo"<div class=\"$class\"> <p>$message</p></div>";

    }
    elseif($response['data']['deleted'])
    {
        $class = "updated";
        $message = 'Deleted';
        //echo"<div class=\"$class\"> <p>$message</p></div>";
        //wp_redirect('admin.php?page=omneo-settings-webhooks-page');
    }
}

function process_webhook_request($wp)
{
    global $wp;

//     echo $_SERVER['REQUEST_URI'] ;

    $url = explode('/', $_SERVER['REQUEST_URI']);

//    /var_dump($url);


    /*$_POST['data'] = '{
        "data": [{
        "id": 4,
        "content_item_id": 4,
        "html": "<p>Hey there!</p>",
        "created_at": "1443413387",
        "updated_at": "1443413387",
        "deleted_at": null,
        "content_item_attributes": {
            "id": 4,
            "content_type": "html",
            "title": "HTML Block 1",
            "description": "HTML Block for Arkade Loyalty Agency",
            "intro_content": null,
            "thumbnail_on_image_url": "http://omneotest1-poc-wp-web.elasticbeanstalk.com/wp-content/uploads/2015/10/600.jpg",
            "thumbnail_off_image_url": "http://omneotest1-poc-wp-web.elasticbeanstalk.com/wp-content/uploads/2015/10/600.jpg",
            "thumbnail_lock_image_url": "http://omneotest1-poc-wp-web.elasticbeanstalk.com/wp-content/uploads/2015/10/600.jpg",
            "allow_comments": 1,
            "allow_likes": 1,
            "allow_ratings": 0,
            "created_at": "1443413387",
            "updated_at": "1443413387",
            "deleted_at": null
            }
        }],
        "elapsed_sec": 0.051949024200439
        }';*/

    $payload = json_decode(stripcslashes($_POST['data']), true);
    //$payload =   stripcslashes($_POST['data']);
    //var_dump(json_last_error());
    write_to_log($payload);


    if($url[1] == 'webhook')
    {
        if($url[2] == 'content')
        {
            switch($url[3])
            {
                case 'html':
                    $data = $payload['data'];
                    \Omneo\Content\Item\Html\webhook($payload);
                    break;

                case 'link':
                    $data = $payload['data'];
                    \Omneo\Content\Item\Links\webhook($payload);
                    break;
            }

        }

        if($url[2] == 'user')
        {
            switch($url[3])
            {
                case 'passwordreset':
                    $data = $payload['data'];
                    password_reset($payload);
                    break;

                case 'activateuser':
                    $data = $payload;
                    user_activation($payload);
                    break;
            }

        }
        
        exit;
    }


    //die();






}
add_action('parse_request', __NAMESPACE__ . '\\process_webhook_request');

/*function custom_rewrite_basic()
{
    add_rewrite_rule('^wh', '?webhook', 'top');
}

add_action('init', __NAMESPACE__ . '\\custom_rewrite_basic');
*/


